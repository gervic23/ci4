const webpack = require('webpack');
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    mode: 'production',
    entry: {
        app: './src/app.js',
        footer: './src/footer.js'
    },

    output: {
        path: path.resolve(__dirname, 'public/dist'),
        filename: 'js/[name].js'
    },

    watchOptions: {
        ignored: [
            'app',
            'public/**', 
            'node_modules/**',
            'admin/**',
            'contributing/**',
            'system/**',
            'test/**',
            'user_guide_src/**',
            'writable/**',
            '.editconfig',
            '.gitignore',
            '.gitattibute',
            '.nokekyll',
            '.travis.yml',
            'CHANGELOG.md',
            'CODE_OF_CONDUCT.md',
            'composer.json',
            'composer.lock',
            'CONTRIBUTING.md',
            'DCO.txt',
            'env',
            'lisense.txt',
            'package.json',
            'package-lock.json',
            'phpdoc.dist.xml',
            'PULL_REQUEST_TEMPLATE.md',
            'README.md',
            'spark',
            'stale.yml',
            'Vagrantfile.dist',
            'webpack.config.js'
        ]
    },

    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },

            {
                test: /\.css$/i,
                use: ["style-loader", 'css-loader', {loader: "MiniCssExtractPlugin.loader"}],
            },

            {
                test: /\.s[ac]ss$/i,
                use: [
                    {
                        'loader': MiniCssExtractPlugin.loader
                    },
                    'css-loader',
                    'sass-loader',
                    "postcss-loader",
                ],
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                type: 'asset/resource',
                generator: {
                    filename: '[name][ext][query]',
                    outputPath: 'fonts/',
                    publicPath: 'dist/fonts/',
                }
            }
        ]
    },

    plugins: [        
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
            chunkFilename: 'css/[id].css',
        }),

        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),

        new webpack.ProvidePlugin({
            Plyr: 'Plyr'
        }),

        new webpack.ProvidePlugin({
            AOS: 'AOS'
        }),

        new webpack.ProvidePlugin({
            stretchTextarea: 'stretchTextarea'
        })
    ]
};